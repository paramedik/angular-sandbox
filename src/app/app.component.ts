import { Component, OnInit } from '@angular/core';
import { element } from 'protractor';
import { HttpClient } from '@angular/common/http';
import { ViewChild } from '@angular/core';
import { ImgMapComponent } from './img-map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  hotspots;
  markersTitles: string[] = [];
  markersPositions: number[][] = [];
  markersUrls: string[] = [];

  @ViewChild('imgMap')
  imgMap: ImgMapComponent;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('./assets/response.json').subscribe(data => {
      this.hotspots = data;
      this.hotspots.forEach(hotspot => {
        this.markersPositions.push([parseInt(hotspot.coord_x), parseInt(hotspot.coord_y)]);
        this.markersTitles.push(hotspot.title);
        this.markersUrls.push(hotspot.alias);
      });
      this.imgMap.draw();
    },
    err => {
      console.log(err);
    });
  }

}
