// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyA7iCvmq8egwJ7ub4BZ6DJprFrGuINKa7k',
    authDomain: 'contact-manager-b1305.firebaseapp.com',
    databaseURL: 'https://contact-manager-b1305.firebaseio.com',
    projectId: 'contact-manager-b1305',
    storageBucket: 'contact-manager-b1305.appspot.com',
    messagingSenderId: '234262762165',
    appId: '1:234262762165:web:fc442064b689147a'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
